﻿using System.ComponentModel.DataAnnotations;

namespace Login_App.Models
{
    public class User
    {
        [Key]
        public required string Email { get; set; }
        public required string FirstName { get; set; }
        public required string LastName { get; set; }
        public int Age { get; set; }
        public required string Password { get; set; }
    }
}
