﻿namespace Login_App.DTOs
{
    public class UpdateUserPasswordDTO
    {
        public string Email { get;set; }
        public string Password { get;set; } 
        public string ConfirmPassword { get; set; }
    }
}
