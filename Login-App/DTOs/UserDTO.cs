﻿namespace Login_App.DTOs
{
    public class UserDTO
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public bool  Valid { get; set; }
    }
}
