﻿using Login_App.DTOs;
using Login_App.Models;
using Login_App.Services;
using Microsoft.AspNetCore.Mvc;
using NuGet.Protocol.Plugins;

namespace Login_App.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserService _userservice;

        public LoginController(IUserService userservice)
        {
            _userservice = userservice;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([Bind("Email,Password")] Login login)
        {
            if (ModelState.IsValid)
            {
                var result=await _userservice.Login(login);
                if (result.Valid)
                {
                    TempData["FirstName"] = result.FirstName;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.msg = "Invalid credentials";
                    return View(login);
                }
                
            }
            return View(login);
        }

        public IActionResult ResetPassword()
        {
            ViewBag.msg = "";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword([Bind("Email,Password,ConfirmPassword")] UpdateUserPasswordDTO user)
        {
            if (ModelState.IsValid)
            {
                var existinguser=await _userservice.GetUserByEmail(user.Email);
                if(existinguser != null)
                {
                    if(user.Password==user.ConfirmPassword)
                    {
                        var login=new Login() { Email = user.Email,Password=user.Password};
                        await _userservice.UpdatePassword(login);
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        ViewBag.msg = "password and confirm password doesn't match ";
                        return View(user);
                    }
                }
                else
                {
                    ViewBag.msg = "user with entered email doesn't exist";
                    return View(user);
                }

            }
            return View(user);
        }

    }
}
