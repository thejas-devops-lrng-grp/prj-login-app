﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Login_App.Data;
using Login_App.Models;
using Login_App.Services;

namespace Login_App.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userservice;

        public UserController(IUserService userservice)
        {
            _userservice = userservice;
        }

        // GET: User
        public async Task<IActionResult> Index()
        {
              return await _userservice.GetUsers() != null ? 
                          View(await _userservice.GetUsers()) :
                          Problem("Entity set 'Login_AppContext.User'  is null.");
        }

        // GET: User/Details/5
        public async Task<IActionResult> Details(string Email)
        {
            if (Email == null || await _userservice.GetUserByEmail(Email) == null)
            {
                return NotFound();
            }

            var user = await _userservice.GetUserByEmail(Email);

            return View(user);
        }

        // GET: User/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Email,FirstName,LastName,Age,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                var result=await _userservice.AddUser(user);
                if(result)
                {
                    return RedirectToAction("Index", "Login");
                }  
                else
                {
                    ViewBag.msg = "User already exists with same email id";
                    return View(user);
                }
            }
            return View(user);
        }

        // GET: User/Edit/5
        public async Task<IActionResult> Edit(string Email)
        {
            if (Email == null ||await _userservice.GetUserByEmail(Email) == null)
            {
                return NotFound();
            }
            var user = await _userservice.GetUserByEmail(Email);
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string Email, [Bind("Email,FirstName,LastName,Age,Password")] User user)
        {
            if (Email != user.Email)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _userservice.UpdateUser(user);
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Email))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: User/Delete/5
        public async Task<IActionResult> Delete(string Email)
        {
            if (Email == null || await _userservice.GetUserByEmail(Email) == null)
            {
                return NotFound();
            }

            var user = await _userservice.GetUserByEmail(Email);
                
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string Email)
        {
            if (await _userservice.GetUsers() == null)
            {
                return Problem("Entity set 'Login_AppContext.User'  is null.");
            }
            if(await _userservice.DeleteUser(Email))
            {
                return RedirectToAction(nameof(Index));
            }
            return NotFound();
        }

        private bool UserExists(string Email)
        {
            var user = _userservice.GetUserByEmail(Email);
          if (user == null)
                return true;
          else
                return false;
        }
    }
}
