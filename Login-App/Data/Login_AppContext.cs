﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Login_App.Models;

namespace Login_App.Data
{
    public class Login_AppContext : DbContext
    {
        public Login_AppContext (DbContextOptions<Login_AppContext> options)
            : base(options)
        {
        }

        public DbSet<Login_App.Models.User> User { get; set; } = default!;
    }
}
