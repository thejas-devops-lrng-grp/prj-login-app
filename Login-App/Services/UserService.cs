﻿using Login_App.Data;
using Login_App.DTOs;
using Login_App.Models;
using Microsoft.EntityFrameworkCore;

namespace Login_App.Services
{
    public class UserService:IUserService
    {
        private readonly Login_AppContext _dbcontext;
        public UserService(Login_AppContext dbcontext)
        {
            _dbcontext=dbcontext;
        }
        public async Task<List<User>> GetUsers()
        {
            return await _dbcontext.User.ToListAsync();
        }
        public async Task<bool> AddUser(User user)
        {
            var existinguser=await GetUserByEmail(user.Email);
            if (existinguser==null)
            {
               await _dbcontext.User.AddAsync(user);
               await _dbcontext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public async Task<User> GetUserByEmail(string Email)
        {
            var user= await _dbcontext.User.FirstOrDefaultAsync(u => u.Email == Email);
            if (user!=null)
            {
                return user;
            }
            return null;   
        }
        public async Task UpdateUser(User user)
        {
            var existinguser = await GetUserByEmail(user.Email);
            if (existinguser != null)
            {
                existinguser.Age = user.Age;
                existinguser.FirstName=user.FirstName;
                existinguser.LastName = user.LastName;
                await _dbcontext.SaveChangesAsync();
            }
        }
        public async Task<bool> DeleteUser(string Email)
        {
            var user=await GetUserByEmail(Email);
            if (user!=null)
            {
                _dbcontext.User.Remove(user);
                await _dbcontext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public async Task<UserDTO> Login(Login login)
        {
            var user=_dbcontext.User.Where(u=>u.Email==login.Email).Where(u=>u.Password==login.Password).FirstOrDefault();
            var userDTO = new UserDTO() { Valid = false };
            if (user!=null)
            {
                
                userDTO.Email = login.Email;
                userDTO.FirstName = user.FirstName;
                userDTO.Valid=true; 
                return userDTO;
            }
            else
            {
                return userDTO;
            }
           
        }
        public async Task<bool> UpdatePassword(Login login)
        {
            var user = await GetUserByEmail(login.Email);
            if(user!=null)
            {
                user.Password = login.Password;
                await _dbcontext.SaveChangesAsync();
                return true;
            }
            else 
            { 
                return false; 
            }  
            
            
        }
    }
}
