﻿using Login_App.DTOs;
using Login_App.Models;

namespace Login_App.Services
{
    public interface IUserService
    {
        public Task<List<User>> GetUsers();
        public Task<bool> AddUser(User user);
        public Task<User> GetUserByEmail(string Email);
        public Task UpdateUser(User user);
        public Task<bool> DeleteUser(string Email);
        public Task<UserDTO> Login(Login login);
        public Task<bool> UpdatePassword(Login login);

    }
}
